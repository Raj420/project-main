package com.testing2;

class Bike{
	int speed=100;
}

class StaticKeyword extends Bike{
	int speed=200;
	
	void display(){
		System.out.println(super.speed);
	}
	
	public static void main(String[] args) {
		StaticKeyword s=new StaticKeyword();
		s.display();
	}
}